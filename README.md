# edeet - web based markdown editor

There's a live build up [here](https://edeet.rhg135.com).

## Features

- modal
- has two panes and one is toggleable
- starts in _INSERT_ mode
- in insert mode, the keymaps are whatever your browser's are, except escape goes to _NORMAL_ mode
- normal mode
  - `p` toggles the preview pane
  - `s` launches a download defaulting to the name `README.md` and consisting of the contents of the editor
  - `o` opens a file chooser
  - `c` clears the editor
  - After any key press, returns to _INSERT_
- Preview uses markdown-it

## Building

I develop with `pnpm` and deploy with `npm` but `yarn` (v1) **SHOULD** work.

### `pnpm`

```sh
pnpm i
pnpm build
```

or via npx:

```sh
npx pnpm i
npm run dev
```

### `npm`

```sh
npm i
npm run build
```

### `yarn` (v1)

```sh
yarn
yarn run build
```

## Hopes And Dreams

- mobile support
  - android, can't test iOS nor build
- automatic io (not prompting for everything)
- an explorer/stats pane
- handle frontmatter (YAML) better- syntax highlighting
- custom styles
- settings
- WYSIWYG mode

## License

MIT, see [the license](./LICENSE). Future contributors see [here](./CONTRIBUTING.md) and to see past contributors see [here](./AUTHORS.md)
