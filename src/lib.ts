import markdownIt, { Options } from 'markdown-it'
import { computed, ref } from 'vue'
import yaml from 'js-yaml'

export const enum EditorMode {
  CODE,
  WYSIWYG
}

export const enum Mode {
  INSERT = 'INSERT',
  NORMAL = 'NORMAL'
}

const defaultMdOpts = { html: true, linkify: true, typographer: true }

/**
 * A replacement for `gray-matter` because it's always a bitch on web
 *
 * @param s markdown in a string
 * @param opts an object with a `.yaml` key to pass to js-yaml
 *  and `.mdIt` to merge into our default options for that
 * @returns an object with one key and one optional key
 *  `.html` the markdown rendered
 *  `.frontmatter` the frontmatter run through js-yaml, optional
 */
export function useMarkdown(
  s: string,
  opts?: { yaml?: yaml.LoadOptions; mdIt?: Options }
) {
  const processor = markdownIt({ ...defaultMdOpts, ...opts?.mdIt })
  if (s.startsWith('---\n')) {
    const endIndex = s.substring(4).indexOf('\n---')
    const stringFrontmatter = s.substring(4, endIndex)
    const frontmatter = yaml.load(stringFrontmatter)
    const body = s.substring(endIndex + 9)
    const html = processor.render(body)
    return {
      frontmatter:
        typeof frontmatter !== 'object'
          ? undefined
          : <Record<string, unknown>>frontmatter,
      html
    }
  }
  return { html: processor.render(s) }
}
