import { ref } from "vue"

function withTemporaryElement<Name extends keyof HTMLElementTagNameMap>(
    tagName: Name,
    f: (element: HTMLElementTagNameMap[Name]) => Generator
) {
    const element = document.createElement(tagName)
    const g = f(element)
    g.next()
    document.body.appendChild(element)
    g.next()
    setTimeout(() => {
        document.body.removeChild(element)
        g.next()
    }, 0)
}

/**
 * Meant for markdown files
 */
class WebFiles {
    /**
     * Used to access files by prompt.
     * 
     * @param defaultName the default suggested name to save to.
     * @param accept See the input[type=file] accept property
     */
    constructor(private defaultName = 'README.md', private accept = '.md,.markdown') { }

    /**
     * Get a file(s) by prompting.
     * 
     * @param multiple Whether to accept multiple files.
     * @returns A promise for a File if multiple is false, otherwise a Promise for a FileListp
     */
    public getFiles(multiple = false) {
        const self = this
        return new Promise<File | FileList>((res, rej) => {
            withTemporaryElement('input', function* (el) {
                el.type = 'file'
                el.oninput = () => el.files ? multiple ? res(el.files) : res(el.files[0]) : rej(new Error('Failed To Select File(s)'))
                el.accept = self.accept
                el.multiple = multiple
                el.style.display = 'none'
                yield
                el.click()
            })
        })
    }

    /**
     * prompts user to download the text to a location they choose.
     * 
     * @param s string containing text
     */
    public save(s: string) {
        const url = URL.createObjectURL(
            new Blob([s], { type: 'text/markdown' })
        )
        const self = this
        withTemporaryElement('a', function* (a) {
            a.href = url
            a.download = self.defaultName
            a.style.display = 'none'
            yield
            a.click()
            yield
            URL.revokeObjectURL(url)
        })
    }

}

const chromeOpen = (<any>window).showOpenFilePicker

/**
 * Not chrome-specific per-se, but only blink implements the api iirc
 */
class ChromeFiles extends WebFiles {
    private handles: { getFile(): Promise<File>, createWritable(): Promise<any> }[] = []

    /**
     * Can we even use this?
     */
    public static hasApi = Boolean(chromeOpen)

    public async getFiles(multiple = false) {
        const hs = await chromeOpen({ multiple })
        hs.forEach((h: { requestPermission: (arg0: { mode: string }) => any }) => h.requestPermission({ mode: 'readwrite' }))
        this.handles = hs
        return await (multiple ? Promise.all(hs.map((h: { getFile(): Promise<File> }) => h.getFile())) : hs[0].getFile())
    }

    public async reload() {
        if (!this.handles) throw new Error(`No Handle(s) stored`)
        const out: File[] = []
        for (const h of this.handles) out.push(await h.getFile())
        return out
    }

    public async saveToHandle(s: string) {
        if (!this.handles) throw new Error(`No Handle(s) stored`)
        const h = await this.handles[0].createWritable()
        const arrbuf = await h.write(s)
    }
}

export default ChromeFiles.hasApi ? ChromeFiles : WebFiles