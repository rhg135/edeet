# Contributing

All contributors, upon a successful merge, expressly withdraws any copyright claims and the merged code is now under MIT license.

## Attribution

For substantial contributions, that shall be decided both by Myself (the original author) and in the request itself, then all the parties (Myself, the contributors, and any other individuals listed in the [license](./LICENSE)) **SHOULD** discuss the possibility of adding the contributors to the [license](./LICENSE).

## Licensing

Any merge request **MAY** be accompanied by a request to be added to the [author's](./AUTHORS.md) list. An attempt **MAY** be made to add contributors who do not request explicitly.

## Commit Messages

I (Myself) am not strict about it. Messages not in the requested formet **WILL** be accepted without regard to format, but this **MAY** change. Out-of-format commits **MAY** not be merged as they **MAY** require cleanup to be done manually.

This format:

```
this is the title

don't go above 80 chars
that's a general advice thing
This paragraph and below won't
be seen when skimmed.
Use it for details.
```

## This Document

None of these are absolutes, but in order:

- this document
- don't be an ass **AND** ask a licenser who **MAY** merge it
- fork and a licenser **MAY** be so impressed it gets merged
